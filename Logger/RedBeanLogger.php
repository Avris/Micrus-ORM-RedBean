<?php
namespace Avris\Micrus\RedBean\Logger;

use Avris\Micrus\Exception\DatabaseException;
use Avris\Micrus\IO\Logger;

class RedBeanLogger implements \RedBeanPHP\Logger
{
    /** @var Logger */
    protected $logger;

    const DISTINGUISH_ERROR = 'An error occurred: ';

    public function __construct(Logger $logger)
    {
        $this->logger = $logger;
    }

    public function log()
    {
        if ( func_num_args() < 1 ) return;

        $logs = array();
        foreach (func_get_args() as $argument)
        {
            if (is_array($argument)) {
                if (!count($argument)) { continue; }
                $logs[] = $this->clearArgument(print_r($argument, true));
            } else {
                $argument = $this->clearArgument($argument);
                $logs[] = $argument;
                if (substr($argument, 0, strlen(static::DISTINGUISH_ERROR)) == static::DISTINGUISH_ERROR) {
                    throw new DatabaseException(substr($argument, strlen(static::DISTINGUISH_ERROR)));
                }
            }
        }

        $this->logger->log('rb', implode(' | ', $logs));
    }

    protected function clearArgument($argument)
    {
        return preg_replace('/\s+/', ' ', $argument);
    }
}