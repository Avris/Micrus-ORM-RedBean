<?php
namespace Avris\Micrus\RedBean\Model;

abstract class Bean extends \RedBeanPHP\SimpleModel
{

    abstract public function __toString();

    public function getId()
    {
        return $this->id;
    }

}
