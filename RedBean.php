<?php
namespace Avris\Micrus\RedBean;

use RedBeanPHP\Facade;
use Avris\Micrus\App;
use Avris\Micrus\ORM;
use Avris\Micrus\RedBean\Logger\RedBeanLogger;

class RedBean implements ORM
{
    /** @var Facade */
    protected $entityManager;

    public function __construct(App $app, $db)
    {
        $driver = isset($db['driver']) ? $db['driver'] : 'memory';

        $app->getLogger()->log('info', sprintf('Database driver %s', $driver));

        switch ($driver) {
            case 'mysql':
                \R::setup(
                    'mysql:host=' . $db['host'] . ';' .
                    'dbname=' . $db['name'],
                    $db['user'],
                    $db['pass']
                );
                break;
            case 'pgsql':
                \R::setup(
                    'pgsql:host=' . $db['host'] . ';' .
                    'dbname=' . $db['name'],
                    $db['user'],
                    $db['pass'])
                ;
                break;
            case 'sqlite':
                \R::setup('sqlite:' . $app->getRootDir() . '/' . $db['file']);
                break;
            case 'cubrid':
                \R::setup(
                    'cubrid:host=' . $db['host'] . ';' .
                    'port=' . $db['port'].';' .
                    'dbname=' . $db['name'],
                    $db['user'],
                    $db['pass']
                );
                break;
            default:
                \R::setup();
        }

        if ($app->getEnv() == 'dev') {
            try {
                \R::getDatabaseAdapter()->getDatabase()->setDebugMode(3, new RedBeanLogger($app->getLogger()));
            } catch(\Exception $e) {
                $app->getLogger()->log('error', 'Cannot set redbean logger');
            }
        } else {
            if (isset($db['freeze']) && $db['freeze']) {
                \R::freeze(true);
            }
        }
        if (!defined('REDBEAN_MODEL_PREFIX')) {
            define('REDBEAN_MODEL_PREFIX', '\\App\\Model\\');
        }
        \R::setAutoResolve();

        $this->entityManager = new Facade;
    }

    public function __destruct()
    {
        \R::close();
    }

    public function getEntityManager()
    {
        return $this->entityManager;
    }

    public function find($type, $id)
    {
        return $this->entityManager->findOne($type, 'id = ?', array($id));
    }

    public function findAll($type)
    {
        return $this->entityManager->findAll($type);
    }

    public function findBy($type, $attribute, $value)
    {
        return is_array($value) ?
            $this->entityManager->find(
                $type,
                $attribute.' IN (' . $this->entityManager->genSlots($value) . ')',
                $value) :
            $this->entityManager->find($type, $attribute.' = ?', array($value));
    }

    public function findOneBy($type, $attribute, $value)
    {
        return is_array($value) ?
            $this->entityManager->findOne(
                $type,
                $attribute.' IN (' . $this->entityManager->genSlots($value) . ')',
                $value) :
            $this->entityManager->findOne($type, $attribute.' = ?', array($value));
    }

}
